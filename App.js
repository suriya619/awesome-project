import React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from "redux-thunk";
import reducers from './Components/Redux/Reducer';

import Signin from './Components/Signin';
import Home from './Components/Home';
import Menu from './Components/Menu';
import Product from './Components/Product';
import ProductDetails from './Components/ProductDetails';
import About from './Components/About';
import BillingDetails from './Components/BillingDetails';
import Map from './Components/Map';
import Location from './Components/Location';
import AnimationDemo from './Components/Animation';
import AnimationDrag from './Components/AnimationDrag';
import VibrationDemo from './Components/Vibration';
import AnimationSlide from './Components/AnimationSlide';
import GestureTap from './Components/GestureTap';

const Stack = createNativeStackNavigator();
const store = createStore(reducers, applyMiddleware(thunk));


const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="AnimationDemo">
          <Stack.Screen name="AnimationSlide" component={AnimationSlide} />
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="Food" component={Menu} />
          <Stack.Screen name="About" component={About} />
          <Stack.Screen name="Liquor" component={Product} />
          <Stack.Screen name="Liquor Details" component={ProductDetails} />
          <Stack.Screen name="Billing Details" component={BillingDetails} />
          <Stack.Screen name="Map" component={Map} />
          <Stack.Screen name="Location" component={Location} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>

  )
}

export default App;
