import React, { useState, useEffect } from 'react';
import { ScrollView, StyleSheet, Text, } from 'react-native';
import { CustomActivityIndicator, CustomProductCard, CustomAddCartProduct } from './CustomComponents/Custom';
import { isEmpty } from 'lodash';
import { useDispatch, useSelector } from 'react-redux';

export default function ProductDetails(props) {

    const styles = StyleSheet.create({
        container: {
            marginHorizontal: 10
        },
        title: {
            fontSize: 16,
            textTransform: "uppercase",
            fontWeight: "bold",
            padding: 20,
            paddingBottom: 0,
            paddingLeft: 10,
            justifyContent: "center",
            color: "#009688",
        }
    });
    const { navigation } = props;
    const [arr, setArr] = useState([]);
    const [title, setTitle] = useState('');
    const [isLoading, setLoading] = useState(true);
    const ProductDetailsIncDec = useSelector(state => (state.auth.ProductDetailsIncDec))
    useEffect(() => {
        const list = props.route.params?.selectArr;
        const clicked = props.route.params?.clickProduct;
        if (!isEmpty(list && clicked)) {
            setArr(list);
            setTitle(clicked);
            setLoading(false);
        }
    })
    const onPressHandler = () => {
        navigation.navigate('Billing Details')
    }


    return (
        <>
            <CustomActivityIndicator animating={isLoading} />
            <ScrollView style={styles.container}>
                <Text style={styles.title} >{title}</Text>
                {arr.map((x, index) =>
                    <CustomProductCard name={x.title} price={x.price} key={x.id} product={x} />
                )}
            </ScrollView>
            <CustomAddCartProduct onPress={() => onPressHandler()} />
        </>
    )
}