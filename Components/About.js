import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { isEmpty } from 'lodash';
import { useDispatch, useSelector } from 'react-redux';
import { CustomUserAbout, CustomLogoutCard } from './CustomComponents/Custom';



const About = (props) => {
    const { navigation } = props;
    console.log(navigation, 'navigation')
    const styles = StyleSheet.create({
        container: {
            marginHorizontal: 10,
            paddingTop: 10,
        },
    });
    onLogout = () => {
        navigation.navigate('Sign In')
        // alert('ghfgh')
    }

    const userAbout = useSelector(state => (state.auth.UsersDataList.user));
    console.log(userAbout, 'userNameuserName');

    return (
        <>
            <View style={styles.container}>
                <CustomUserAbout data={userAbout} />
                <CustomLogoutCard onPress={() => onLogout()} />
            </View>
        </>
    )
}
export default About;