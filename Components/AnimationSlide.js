import React, { Component, useState } from 'react';
import { View, Text, StyleSheet, SafeAreaView, FlatList, Animated, } from 'react-native';
import { Swipeable, GestureHandlerRootView } from 'react-native-gesture-handler';
import { Divider, NativeBaseProvider } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialIcons';

const AnimationSlide = () => {

  const DATA = [
    {
      id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
      title: 'First Item',
    },
    {
      id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
      title: 'Second Item',
    },
    {
      id: '58694a0f-3da1-471f-bd96-145571e29d72',
      title: 'Third Item',
    },
    {
      id: '58694a0f-3da1-471f-bd96-145571e29d22',
      title: 'Fourth Item',
    },
    {
      id: '58694a0f-3da1-471f-bd96-145571e29d522',
      title: 'Fifth Item',
    },
    {
      id: '58694a0f-3da1-471f-bd96-145571e29d722',
      title: 'Sixth Item',
    },
  ];

  const [arr, setArr] = useState(DATA);
  const [refreshing, setrefreshing] = useState(false);

  const onRefresh = () => {
    setrefreshing(true);
    setTimeout(() => {
      setArr((data) => [
        ...data,
        {
          id: '57878',
          title: 'Take a walk in the park',
        },
      ]);
      setrefreshing(false);
    }, 2000);
  }

  const handleSwipeLeft = (progress, dragX) => {
    const scale = dragX.interpolate({
      inputRange: [0, 100],
      outputRange: [0, 1],
      extrapolate: 'clamp'
    })
    return (
      <View style={{ flex: 1, backgroundColor: 'red', alignItems: 'flex-start', justifyContent: 'center', paddingLeft: 20 }}>
        {/* <Animated.Text style={{ flex: 1, justifyContent: 'center', transform: [{ scale }] }}>add to checklist</Animated.Text> */}
        <Icon name='delete' size={24} color='white' />
      </View>
    )
  }
  const left = () => {
    console.log('left')
  }

  return (

    <View style={{ paddingHorizontal: 20, paddingVertical: 20 }}>
      <FlatList
        data={arr}
        renderItem={({ item }) => {
          return (
            <NativeBaseProvider >
              <GestureHandlerRootView>
                <Swipeable
                  renderLeftActions={handleSwipeLeft}
                  onSwipeableOpen={left}>
                  <View style={{ paddingHorizontal: 20, paddingVertical: 20, backgroundColor: 'white' }}>
                    <Text style={{ color: 'black' }}>{item.title}</Text>
                  </View>
                </Swipeable>
              </GestureHandlerRootView>
              <Divider mX={2} />
            </NativeBaseProvider>

          )
        }}
        keyExtractor={item => item.id}
        refreshing={refreshing}
        onRefresh={onRefresh}
      />
    </View>
  );
};

export default AnimationSlide;