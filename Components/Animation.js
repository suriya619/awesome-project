import React, { useState } from "react";
import { Platform, StyleSheet, Text, View, Dimensions, TouchableOpacity, SafeAreaView, Animated, Easing, Button } from "react-native";
import * as Animatable from "react-native-animatable";


const AnimationDemo = (props) => {

  const [type, setType] = useState("");
  const [count, setCount] = useState(0);
  const [sty, setSty] = useState(1);
  const [btnDisable, setBtnDisable] = useState(false);

  const onAnimationBegin = () => {
    setBtnDisable(true);
    setSty(0);
  }
  const onAnimationEnd = () => {
    setCount(0);
    setType("");
    setSty(1);
    setBtnDisable(false);
  }
  const handleBounce = () => {
    setCount(1);
    setType("bounceIn")
  }
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Animatable.View
        animation={type}
        iterationCount={count}
        direction="normal"
        delay={0}
        onAnimationBegin={onAnimationBegin}
        onAnimationEnd={onAnimationEnd}
        style={styleSheet.MainContainer}>
        <Text style={sty ? styleSheet.beforetext : styleSheet.aftertext}>Text Up Down With Animation</Text>
      </Animatable.View>
      <Button title="Bounce" onPress={handleBounce} disabled={btnDisable} ></Button>
    </View>
  )
}

const styleSheet = StyleSheet.create({
  MainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  beforetext: {
    fontSize: 16,
    color: 'grey',
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 10
  },
  aftertext: {
    fontSize: 16,
    color: 'grey',
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 10,
    shadowColor: 'rgba(0, 0, 0, 0.2)',
    elevation: 20
  }
});

export default AnimationDemo;