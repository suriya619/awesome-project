import React, { useState, useEffect } from "react";
import { StyleSheet, View, Text, Button, TextInput } from 'react-native';
import { connect, useDispatch } from "react-redux";
import {
    CustomTextField, CustomButton, CustomActivityIndicator, CustomAddCartProduct, CustomViewCard, CustomPayments, CustomViewCardTotal,
    CustomCashPayments, CustomAlert, CustomDatePicker
} from './CustomComponents/Custom';
import { isEmpty } from 'lodash';
import { SigninUser } from './Redux/Action/SigninAction';
import Icon from 'react-native-vector-icons/MaterialIcons';


const Signin = (props) => {


    const styles = StyleSheet.create({
        container: {
            marginHorizontal: 10,
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
        },
        input: {
            flex: 1,
            paddingTop: 10,
            paddingRight: 10,
            paddingBottom: 10,
            paddingLeft: 0,
            backgroundColor: '#fff',
            color: '#424242',
        },
    });

    const dispatch = useDispatch();
    const { navigation } = props;
    console.log(props, 'propspropsprops')
    const reqObject = {
        username: "",
        password: "",
    };
    const [isLoading, setLoading] = useState(false);
    const [btnDisable, setBtnDisable] = useState(false);
    const [form, setForm] = useState(reqObject);
    const months = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'july', 'august', 'sep', 'oct', 'nov', 'dec', 'jan', 'feb', 'mar', 'apr', 'may', 'jun', 'july', 'august', 'sep', 'oct', 'nov', 'dec', 'jan', 'feb', 'mar', 'apr', 'may', 'jun', 'july', 'august', 'sep', 'oct', 'nov', 'dec', 'jan', 'feb', 'mar', 'apr', 'may', 'jun', 'july', 'august', 'sep', 'oct', 'nov', 'dec'];

    const day = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];
    const handleChangeValue = (value, type) => {
        setForm({ ...form, [type]: value });
    };
    const submitHandler = async (event) => {
        if (!isEmpty(form)) {
            navigation.navigate('Home')

            // setBtnDisable(true)
            // setLoading(true)
            // event.preventDefault();
            // const fetch_APIlogin = await dispatch(SigninUser(form));
            // console.log(fetch_APIlogin, 'fetch_APIlogin')
            // if (fetch_APIlogin["status"] === 200) {
            //     setForm(reqObject)
            //     setLoading(false)
            //     setBtnDisable(true)
            //     navigation.navigate('Home')
            // } else {
            //     setForm(reqObject)
            //     setLoading(false)
            //     setBtnDisable(true)
            //     alert('Invalid Email or Password !')
            // }
        }
        else[
            setLoading(false),
            setBtnDisable(true),
            alert('Email and Password Required')
        ]
    }

    return (
        <>
            <CustomActivityIndicator animating={isLoading} />
            <View style={styles.container}>
                <View>
                    <CustomTextField onChangeText={(e) => handleChangeValue(e, "username")} value={form.username} placeholder="Email" placeholderTextColor="grey"></CustomTextField>
                    <CustomTextField onChangeText={(e) => handleChangeValue(e, "password")} value={form.password} placeholder="Password" placeholderTextColor="grey" secureTextEntry={true}></CustomTextField>
                    <CustomButton onPress={submitHandler} text={'Sign In'} />
                </View>
            </View>
            <View style={styles.container}>
                <CustomDatePicker dataList={months} />
            </View>

            {/* <View style={{
                height: 150,
                borderColor: 'gray',
                borderWidth: 1,
                justifyContent: 'center',
                backgroundColor: 'grey',
                opacity: 1,
            }}>
                <View style={{
                    height: 50,
                    borderColor: 'black',
                    borderWidth: 1,
                    backgroundColor: 'black',
                    zIndex: 5,
                    position: 'relative'
                }}>

                </View>
            </View> */}

            <View>
                {months.map((mo) => {
                    return (
                        <View >
                            <Text style={{ color: 'black', fontSize: 16, padding: 5, }}>{mo}</Text>
                        </View>
                    )
                })}
            </View>
        </>
    )
}


export default Signin