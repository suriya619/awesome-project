import React, { useRef, useState } from "react";
import { Platform, StyleSheet, Text, View, Dimensions, TouchableOpacity, SafeAreaView, Animated, Easing, Button, PanResponder } from "react-native";
import * as Animatable from "react-native-animatable";


const AnimationDrag = () => {
  const pan = useRef(new Animated.ValueXY()).current;


  const [currentPosition, setCurrentPosition] = useState();
  console.log(pan, 'pan')

  const panResponder = useRef(
    PanResponder.create({
      onMoveShouldSetPanResponder: () => true,
      onPanResponderMove: Animated.event([null, { dx: pan.x, dy: pan.y }]),
      onPanResponderRelease: () => {
        Animated.spring(pan, {
          toValue: { x: 0, y: 0 },
          useNativeDriver: true,
        }).start();
      },
    }),
  ).current;

  return (
    <View style={styles.container}>
      <Animated.View
        style={{
          transform: [{ translateX: pan.x }, { translateY: pan.y }],
        }}
        {...panResponder.panHandlers}>
        <View style={styles.box} />
      </Animated.View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    height: 150,
    width: 150,
    backgroundColor: 'grey',
    borderRadius: 5,
  },
}
)

export default AnimationDrag

