import React, { useRef, useState } from 'react';
import { View, Text, StyleSheet, SafeAreaView, FlatList, Animated, } from 'react-native';
import { TapGestureHandler, State, GestureHandlerRootView, LongPressGestureHandler, PinchGestureHandler } from 'react-native-gesture-handler';


const GestureTap = () => {

  const [likeColour, setLikeColour] = useState('#28b5b5');
  const doubleTapRef = useRef(null);

  const onDoubleTapEvent = (event) => {
    if (event.nativeEvent.state === State.ACTIVE) {
      likeColour === '#28b5b5'
        ? setLikeColour('red')
        : setLikeColour('#28b5b5');
    }
  };

  const onSingleTapEvent = (event) => {
    if (event.nativeEvent.state === State.ACTIVE) {
      alert('Hey single');
    }
  };

  const onLongPress = (event) => {
    if (event.nativeEvent.state === State.ACTIVE) {
      alert("I've been pressed for 500 milliseconds");
    }
  };

  const styles = StyleSheet.create({
    square: {
      width: 150,
      height: 150,
      backgroundColor: likeColour,
      marginTop: 22,
      marginBottom: 22,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 0,
      }
    }
  })

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <GestureHandlerRootView>
        {/* <TapGestureHandler
          ref={doubleTapRef}
          onHandlerStateChange={onSingleTapEvent}
          numberOfTaps={1}
        >
          <View style={styles.square} />
        </TapGestureHandler> */}

        <LongPressGestureHandler
          onHandlerStateChange={onLongPress}
          minDurationMs={500}
        >
          <View style={styles.square} />
        </LongPressGestureHandler>
      </GestureHandlerRootView>
    </View>
  )
}

export default GestureTap;