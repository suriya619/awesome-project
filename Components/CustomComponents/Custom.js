import { Header } from "@react-navigation/stack";
import React, { useState } from "react";
import { StyleSheet, Text, TextInput, TouchableOpacity, ActivityIndicator, View, Image, Button, ScrollView, } from 'react-native';
import { useDispatch, useSelector, connect } from 'react-redux';
import { capture_getProduct_IncDec, setCart, setComponentType } from '../Redux/Action/SigninAction';
import { Picker } from '@react-native-picker/picker';
import Icon from 'react-native-vector-icons/MaterialIcons';


const styles = StyleSheet.create({
    primaryColor: {
        backgroundColor: "#009688",
    },
    primaryBorderColor: {
        borderColor: "#009688",
    },
    primaryTextColor: {
        color: "#009688",
    },
    input: {
        height: 50,
        margin: 12,
        borderWidth: 1,
        color: "#121212",
        padding: 10,
        width: 300,
        borderRadius: 5,
        fontSize: 16,
    },
    appButtonContainer: {
        justifyContent: "center",
        padding: 16,
        width: 300,
        margin: 12,
        borderRadius: 5,
    },
    appButtonText: {
        fontSize: 18,
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase",
    },
    spinnerView: {
        alignSelf: "center",
        position: 'absolute',
        justifyContent: "center",
        zIndex: 999,
        flex: 1,
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        opacity: 0.5,
        backgroundColor: 'black',
        alignItems: 'center'
    },
    font: {
        fontSize: 20,
    },
    item: {
        elevation: 0.5,
        backgroundColor: "white",
        padding: 10,
        marginVertical: 8,
        borderRadius: 5,
    },
    cardText: {
        fontSize: 16,
        fontWeight: "500",
        textTransform: "uppercase",
        margin: 10,
        flex: 1,
    },
    productImage: {
        height: null,
        flex: 1,
        width: '100%',
    },
    cardBodyL: {
        marginTop: 20,
        elevation: 0.5,
        width: 160,
        height: undefined,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0,
        backgroundColor: "white",
        marginHorizontal: 0,
    },
    cardBodyR: {
        marginTop: 20,
        elevation: 0.5,
        width: 200,
        height: undefined,
        borderRadius: 5,
        backgroundColor: "white",
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
        marginHorizontal: 0,
        justifyContent: 'space-around',
    },
    cardBodyBg: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    logo: {
        width: 170,
        height: 200,
        backgroundColor: "grey",
    },
    addButtonContainer: {
        justifyContent: "center",
        padding: 16,
        width: 170,
        margin: 12,
        borderRadius: 5,
    },
    addButtonText: {
        fontSize: 18,
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase",
    },
    ThreeButtonContainer: {
        margin: 12,
        width: 170,
        height: 56,
        flexDirection: 'row',
    },
    IncButtonContainer: {
        justifyContent: "center",
        height: 56,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
        flex: 1,
    },
    DecButtonContainer: {
        justifyContent: "center",
        height: 56,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
        flex: 1,
    },
    PriceButtonContainer: {
        backgroundColor: "#f6fffe",
        justifyContent: "center",
        width: 170,
        height: 56,
        borderRadius: 0,
        flex: 1,
    },
    priceTest: {
        fontSize: 16,
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase",
    },
    aboutContainer: {
        backgroundColor: "white",
        padding: 16,
        marginTop: 10,
        borderRadius: 5,
        flexDirection: 'column',
        elevation: 0,
        marginVertical: 8,
    },
    aboutHeader: {
        fontSize: 12,
        paddingTop: 0,
        color: "#5a5a5a",
        fontWeight: "normal",
        textTransform: "uppercase",
    },
    aboutContent: {
        fontSize: 16,
        paddingTop: 8,
        color: "#5a5a5a",
        fontWeight: "bold",
        textTransform: "uppercase",
    },
    logouttContent: {
        fontSize: 16,
        color: "red",
        fontWeight: "bold",
        textTransform: "uppercase",
    },
    cartContainer: {
        borderRadius: 5,
        elevation: 8,
        marginVertical: 8,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: 10,
        marginRight: 10,
    },
    cartButtonContainer: {
        justifyContent: "center",
        padding: 8,
        width: undefined,
        margin: 12,
        borderRadius: 5,
    },
    viewcartButtonContainer: {
        backgroundColor: "white",
        justifyContent: "center",
        padding: 8,
        width: undefined,
        margin: 12,
        borderRadius: 5,
    },
    cartButtonText: {
        fontSize: 14,
        color: "#fff",
        fontWeight: "normal",
        alignSelf: "center",
        textTransform: "uppercase",
    },
    cartButtonAmount: {
        fontSize: 16,
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase",
    },
    viewcartButtonAmount: {
        fontSize: 16,
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase",
    },
    viewCartBill: {
        backgroundColor: "white",
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    paymentBillTotal: {
        backgroundColor: "white",
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderStyle: 'dashed',
        borderWidth: 1,
        backgroundColor: '#fbffff',
    },
    paymentBillContainer: {
        elevation: 0.5,
        backgroundColor: "white",
        padding: 10,
        borderRadius: 5,
        flexDirection: 'column',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
    },
    onlinePaymentsContainer: {
        elevation: 0.5,
        backgroundColor: "white",
        padding: 20,
        borderRadius: 5,
        flexDirection: 'column',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
    },
    onlinePaymentsList: {
        backgroundColor: "white",
        paddingTop: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        // marginRight: 10,
        marginTop: 10,
    },
    circleSelected: {
        width: 15,
        height: 15,
        borderRadius: 15,
    },
    circleNotSelected: {
        width: 15,
        height: 15,
        borderRadius: 15,
        backgroundColor: "#ebeaea",
    },
    onlinePaymentsHeader: {
        padding: 20,
        paddingBottom: 0,
        paddingLeft: 10,
        fontSize: 16,
        fontWeight: "bold",
        textTransform: "uppercase",
        justifyContent: 'flex-start'
    },
    alertCard: {
        width: '80%',
    },
    alertType: {
        // width: '80%',
        // height: 50,
        backgroundColor: "white",
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        borderRadius: 0,
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
    },
    alertbtncontainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: "white",
        width: '80%',
        borderRadius: 0,
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
    },
    alertheader: {
        backgroundColor: "white",
        justifyContent: 'flex-start',
        padding: 20,
        height: 90,
        width: '80%',
        borderRadius: 0,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
    },
    alertBackgroundView: {
        alignSelf: "center",
        position: 'absolute',
        justifyContent: "center",
        zIndex: 998,
        flex: 1,
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        opacity: 0.5,
        backgroundColor: 'black',
        alignItems: 'center',
    },
    alertOptionText: {
        fontSize: 14,
        fontWeight: '500',
        textTransform: "uppercase",
    },
    alertOptionHeaderText: {
        fontSize: 18,
        fontWeight: '500',
    }
});

const dispatchData = useDispatch();


const CustomDatePicker = (props) => {
    const { dataList } = props;
    const [DatePicker, setDatePicker] = useState(false);
    const [monthValue, setMonthValue] = useState('');
    const selectMonth = (month) => {
        setDatePicker(false)
        setMonthValue(month)
    }


    return (
        <>
            {DatePicker ?
                <View style={{
                    height: 130, width: 300, backgroundColor: '#e8e8e8', borderRadius: 10,
                }}>
                    <ScrollView showsVerticalScrollIndicator={false} style={{ height: 120, width: 300, }}>
                        {(dataList || []).map((month, i) => {
                            return (
                                <View style={{ alignItems: 'center', justifyContent: 'center' }} key={i}>
                                    <Text style={{ color: 'black', fontSize: 16, padding: 5 }} onPress={() => selectMonth(month)}>{month}</Text>
                                </View>
                            )
                        })}
                    </ScrollView>
                </View> :
                <TouchableOpacity onPress={() => setDatePicker(true)}>
                    <View style={{
                        height: 50, width: 300, backgroundColor: '#e8e8e8', borderRadius: 10, alignItems: 'center', justifyContent: 'center'
                    }}>
                        <Text style={{ color: 'black', fontSize: 16 }} >{((monthValue === '') ? dataList[0] : monthValue)}</Text>
                    </View>
                </TouchableOpacity>
            }
        </>
    )
}

const CustomAlert = (props) => {
    return (
        <>
            {/* <View style={{ alignItems: 'center', flex: 1, justifyContent: 'center', zIndex: 999, position:'absolute' }}>
            </View> */}


            <View style={{ justifyContent: 'center', zIndex: 9999 }}>
                <View style={styles.alertBackgroundView}>
                </View>
                <View style={{ zIndex: 999, alignItems: 'center' }}>
                    <View style={styles.alertheader}>
                        <Text style={styles.alertOptionHeaderText}>Alert message</Text>
                    </View>
                    <View style={styles.alertbtncontainer}>
                        <View style={styles.alertType}>
                            <TouchableOpacity>
                                <View ><Text style={styles.alertOptionText}>no</Text></View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.alertType}>
                            <TouchableOpacity>
                                <View ><Text style={styles.alertOptionText}>yes</Text></View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        </>
    )
}

const CustomCashPayments = (props) => {
    const { onPress } = props;
    const [showOptions, setShowOptions] = useState(false);
    return (
        <>
            <View>
                <Text style={[styles.onlinePaymentsHeader, styles.primaryTextColor]}>cash</Text>
                <View style={styles.onlinePaymentsContainer} >
                    <TouchableOpacity onPress={() => setShowOptions(!showOptions)} activeOpacity={0} style={{ justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ fontWeight: '600', color: 'grey' }}>Pay by Cash</Text>
                        {showOptions ? <View style={[styles.circleSelected, styles.primaryColor]} /> : <View style={styles.circleNotSelected} />}
                    </TouchableOpacity>
                </View>
            </View>
        </>
    )
};
const CustomPayments = (props) => {
    const { data = [], value = {}, onSelect = () => { } } = props;
    const Type = (data[0]?.type);
    const Placeholder = (data[0]?.palceholder);
    const [showOptions, setShowOptions] = useState(false);
    const [show, setShow] = useState(false)
    const [selected, setSelected] = useState(false);


    const onSelectOptions = (x) => {
        setShowOptions(false);
        setSelected(true)
        onSelect(x);
        setShow(false)
    }
    return (
        <>
            <View>
                <Text style={[styles.onlinePaymentsHeader, styles.primaryTextColor]}>{Type}</Text>
                <View style={styles.onlinePaymentsContainer}>
                    <TouchableOpacity onPress={() => setShowOptions(!showOptions)} style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                        <Text style={{ fontWeight: '600', color: 'grey' }}>{value ? value?.label : Placeholder}</Text>
                        {value?.id ? <View style={[styles.circleSelected, styles.primaryColor]} /> : null}
                    </TouchableOpacity>
                    {showOptions && (<View>
                        {data.map((x, index) => {
                            return (
                                <TouchableOpacity key={index} onPress={() => onSelectOptions(x)} style={styles.onlinePaymentsList} >
                                    <Text style={{ color: 'grey' }} >{x.label}  <Text>{x.accNo} </Text></Text>
                                    {value.id === x.id ? <View style={[styles.circleSelected, styles.primaryColor]} /> : <View style={styles.circleNotSelected} />}
                                </TouchableOpacity>
                            )
                        })}
                    </View>)}
                </View>
            </View>
        </>
    )
};
const CustomViewCard = (props) => {
    const { item, price, quantity } = props;
    return (

        <View style={styles.viewCartBill}>
            <Text style={{ color: 'grey' }} >{item} </Text>
            <Text style={{ color: 'grey' }} >{quantity}  *  {price} </Text>
        </View>
    )
};
const CustomViewCardTotal = (props) => {
    return (
        <View style={[styles.paymentBillTotal, styles.primaryBorderColor]}>
            <Text style={[styles.viewcartButtonAmount, styles.primaryTextColor]}>Total</Text>
            <Text style={[styles.viewcartButtonAmount, styles.primaryTextColor]}>$ 21321 </Text>
        </View>
    )
};
const CustomTextField = (props) => {
    return (
        <TextInput style={[styles.input, styles.primaryBorderColor]}  {...props} />
    )
};
const CustomButton = (props) => {
    const { onPress, text, disabled } = props;
    return (
        <TouchableOpacity style={[styles.appButtonContainer, styles.primaryColor]} onPress={onPress} disabled={false} >
            <Text style={styles.appButtonText} >{text}</Text>
        </TouchableOpacity>
    )
};
const CustomActivityIndicator = (props) => {
    const { animating } = props;

    if (!animating) return false;

    return (
        <View style={styles.spinnerView}>
            <ActivityIndicator animating={animating} size={50} color="white"></ActivityIndicator>
        </View>
    )
};
const CustomCard = (props) => {
    const { text, onPress } = props
    return (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.item}>
                <Text style={[styles.cardText, styles.primaryTextColor]}>{text}</Text>
            </View>
        </TouchableOpacity>
    )
};
const CustomProductCard = (props) => {
    const [btn, setBtn] = useState(false);
    const [count, setCount] = useState(0);
    const { name, price, imageURL, onPressADD, onPressSUB, onPressBTN, product } = props;
    const [amount, setAmount] = useState(0);

    const onButtonClick = () => {
        setBtn(true);
        setCount(1);
        setAmount(price * 1);
        dispatchData((setCart(true)))
        const temp = (Array(product) ? Array(product) : []).map((item) => {
            return {
                ...item,
                quantity: count,
                productamount: price * 1,
            }
        })
        dispatchData((capture_getProduct_IncDec(temp)))
    }
    const onAddClick = () => {
        setCount(count + 1);
        setAmount(price * count);
        const temp = (Array(product) ? Array(product) : []).map((item) => {
            return {
                ...item,
                quantity: count + 1,
                productamount: price * count,
            }
        })
        dispatchData((capture_getProduct_IncDec(temp)))
    }
    const onSubClick = () => {
        if (count == 0) {
            setBtn(false);
            setAmount(0)
        }
        else if (count >= 1) {
            setCount(count - 1)
            setAmount(price * count);
            const temp = (Array(product) ? Array(product) : []).map((item) => {
                return {
                    ...item,
                    quantity: count - 1,
                    productamount: price * count,
                }
            })
            dispatchData((capture_getProduct_IncDec(temp)))
        }
    }
    return (<>
        <View style={styles.cardBodyBg}>
            <View style={styles.cardBodyL}>
                <Image style={styles.productImage} source={{ uri: imageURL }}
                />
            </View>
            <View style={styles.cardBodyR}>
                <Text style={styles.cardText}>{name}</Text>
                <Text style={{ fontSize: 16, color: "#009688", fontWeight: "500", textTransform: "uppercase", margin: 10, flex: 1, }} >$ {price}</Text>
                <Text style={{ fontSize: 20, color: "#009688", fontWeight: "500", textTransform: "uppercase", margin: 10, flex: 1, }}>$ {amount}</Text>

                {(!btn) ?
                    <TouchableOpacity style={[styles.addButtonContainer, styles.primaryColor]} onPress={() => onButtonClick()} >
                        <Text style={styles.addButtonText} onPressBTN={onPressBTN}>ADD</Text>
                    </TouchableOpacity> :

                    <View style={styles.ThreeButtonContainer}>
                        <TouchableOpacity style={[styles.DecButtonContainer, styles.primaryColor]} onPressSUB={onPressSUB} onPress={() => onSubClick()}>
                            <Text style={styles.addButtonText} >-</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.PriceButtonContainer}>
                            <Text style={[styles.priceTest, styles.primaryTextColor]} >{count}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.IncButtonContainer, styles.primaryColor]} onPressADD={onPressADD} onPress={() => onAddClick()} >
                            <Text style={styles.addButtonText} >+</Text>
                        </TouchableOpacity>
                    </View>
                }
            </View>
        </View>
    </>
    )
};
const CustomAddCartProduct = (props) => {
    const { onPress } = props;
    const cartView = useSelector(state => state.auth.cartView)
    if (!cartView) {
        return false;
    }
    return (
        <View style={[styles.cartContainer, styles.primaryColor]} >
            <View>
                <TouchableOpacity style={[styles.cartButtonContainer, styles.primaryColor]} disabled={false} >
                    <Text style={styles.cartButtonText} >Item<Text style={styles.cartButtonAmount} > 1</Text>  |  <Text style={styles.cartButtonText} >To Pay</Text> <Text style={styles.cartButtonAmount} >$ 100</Text></Text>
                </TouchableOpacity>
            </View>
            <View>
                <TouchableOpacity style={styles.viewcartButtonContainer} disabled={false} onPress={onPress}>
                    <Text style={styles.viewcartButtonAmount}>View Cart</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
};
const CustomUserAbout = (props) => {
    const { data } = props;
    return (
        <>
            <View style={styles.aboutContainer}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View>
                        <Text style={styles.aboutHeader} >userName</Text>
                        <Text style={styles.aboutContent} >{`${data?.userName}`}</Text>
                    </View>
                    <View>
                        <Icon name='create' size={24} color='#777' />
                    </View>
                </View>
            </View>
            <View style={styles.aboutContainer}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View>
                        <Text style={styles.aboutHeader} >firstName</Text>
                        <Text style={styles.aboutContent} >{`${data?.firstName}`}</Text>
                    </View>
                    <View>
                        <Icon name='create' size={24} color='#777' />
                    </View>
                </View>
            </View>
            <View style={styles.aboutContainer}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View>
                        <Text style={styles.aboutHeader} >contact</Text>
                        <Text style={styles.aboutContent} >{`${data?.email}`}</Text>
                    </View>
                    <View>
                        <Icon name='create' size={24} color='#777' />
                    </View>
                </View>
            </View>
            <View style={styles.aboutContainer}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View >
                        <Text style={styles.aboutHeader} >gender</Text>
                        <Text style={styles.aboutContent} >{`${data?.gender}`}</Text>
                    </View>
                    <View>
                        <Icon name='create' size={24} color='#777' />
                    </View>
                </View>

            </View>
            <View style={styles.aboutContainer}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View>
                        <Text style={styles.aboutHeader} >city</Text>
                        <Text style={styles.aboutContent} >{`${data?.city}`}</Text>
                    </View>
                    <View>
                        <Icon name='create' size={24} color='#777' />
                    </View>
                </View>

            </View>
            <View style={styles.aboutContainer}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View >
                        <Text style={styles.aboutHeader} >lastName</Text>
                        <Text style={styles.aboutContent} >{`${data?.lastName}`}</Text>
                    </View>
                    <View>
                        <Icon name='create' size={24} color='#777' />
                    </View>
                </View>
            </View>
        </>
    )
};
const CustomLogoutCard = (props) => {
    const { onPress } = props;
    return (
        <TouchableOpacity style={styles.aboutContainer} onPress={onPress}>
            <View >
                {/* <Text style={styles.aboutHeader} >userName</Text> */}
                <Text style={styles.logouttContent} >logout</Text>
            </View>
        </TouchableOpacity>
    )
};

export {
    CustomTextField, CustomButton, CustomActivityIndicator, CustomCard, CustomProductCard, CustomUserAbout, CustomLogoutCard, CustomAddCartProduct, CustomViewCard,
    CustomPayments, CustomViewCardTotal, CustomCashPayments, CustomAlert, CustomDatePicker
};