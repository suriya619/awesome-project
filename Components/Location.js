import React, { useState, useEffect, useRef } from "react";
import { StyleSheet, View, Text, Button, TouchableOpacity } from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import Icon from 'react-native-vector-icons/MaterialIcons';
import RBSheet from "react-native-raw-bottom-sheet";
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';

const Location = (props) => {

    const styles = StyleSheet.create({
        container: {
            marginHorizontal: 10
        },
        appCurrentButtonContainer: {
            backgroundColor: "#009688",
            justifyContent: "center",
            padding: 16,
            width: 300,
            margin: 12,
            borderRadius: 5,
            // borderColor: '#009688',
            // borderWidth: 1,
        },
        appNewButtonContainer: {
            backgroundColor: "white",
            justifyContent: "center",
            padding: 16,
            width: 300,
            margin: 12,
            borderRadius: 5,
            borderColor: '#009688',
            borderWidth: 1,
        },
        btnContainer: {
            flex: 1,
            justifyContent: 'flex-end',
            alignItems: 'center',
            paddingBottom: 20,
        },
        btnTextCurrent: {
            color: 'white',
            // textAlign: 'center',
            textTransform: "uppercase",
            fontSize: 16,
            fontWeight: "500",
        },
        btnTextNew: {
            color: '#777',
            // textAlign: 'center',
            textTransform: "uppercase",
            fontSize: 16,
            fontWeight: "500",
        },
        map: {
            height: 'auto',
            width: 'auto',
            alignItems: 'stretch',
            alignSelf: 'stretch',
            flex: 1,
        },
        mapContainer: {
            height: 400,
            width: undefined,
            backgroundColor: 'white',
            alignItems: 'center',
            margin: 20,
            marginTop: 20,
            borderRadius: 5,
        },
        mapTextBox: {
            height: 50,
            width: undefined,
            backgroundColor: 'white',
            alignItems: 'center',
            margin: 20,
            marginBottom: 10,
            borderRadius: 5,
        }
    });



    const { navigation } = props;
    const refRBSheet = useRef();
    const latlng = { latitude: 0, longitude: 0 }
    const [mapLocation, setMapLocation] = useState(latlng);

    useEffect(() => {
        refRBSheet.current.open()
    })

    const onCurrentLocation = () => {
        Geolocation.getCurrentPosition((data) => {
            console.log(data, 'data')
            setMapLocation({ latitude: data?.coords?.latitude, longitude: data?.coords?.longitude })
        }, (error) => {
            console.log(error, "error")
        });
        refRBSheet.current.close()
    }

    const onNewLocation = () => {
        const newLocation = true
        navigation.push('Map', mapsview = { newLocation })
    }

    return (
        <>
            {/* <View style={styles.mapContainer}>
                <MapView
                    provider={PROVIDER_GOOGLE}
                    style={styles.map}
                    region={{
                        latitude: mapLocation.latitude,
                        longitude: mapLocation.longitude,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0922,
                    }}>
                    <Marker
                        image={{ uri: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADsQAAA7EB9YPtSQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAkpSURBVHic7Z17jFxVHcc/vzuvfbRpqQYUU9q0KVYrxXa2DdtuF2qgiTw0JlIIVnwlaERJlDT+YUzqHwrKP0YTxIAmKjZAFdSGVOqipbCUpN2WFDbytq0Uqn1v9zHP+/OP3eqwnbm7287cc2bP+fx5zmTu99z7mXPvuffMucI0Rzs755CkG7gCCRaDXg76PpDZwIwQUqESllRKuTAxkFN562Qx+cxQSXoz5czOpS89edJ0GxqJmA7QCPSazsWEsgH4JPBxIJjaNwiDZeFoIR2eLCf2qeg2leTDK/b85dUGxDXKtBFAs9kUMzIbUL4GrKzX9+Y04HA+zalSAuAFRB6A45s7+vqK9dqGSZpeAM1mU7Sn70BlI8K8Rm3nZCnBoXyGkgrAAUR/DCcfanYRmloA7V7VjXA/ypI4tlcMhTdzGYbCxGiB8JqGeueKfT09cWy/ETSlALpuaTsjM36G8EVibkMI/DPXcvaUAKAIv4KWuzr6tg7HmaUeNJ0A2n3VRyB4DPiYsQzAW7kMp0rJyuJXEF3f0dfzkqFY58UUr47NomtW3QDBHgwefBj91SxoKTAzUa4sXozKrt0d111vKNZ50TQC6JpVGxCeANpMZwEQlAUteTJBWFncLiF/2r1s3VdM5ZoqTSGAdnd+GeE3QMp0lkqSoixoKYw/jyZF9MG+5dd+yUyqqWH9NYBe3XUTGj4OJCf8sCHeLaR4p5AeX1wWdH12b8/jJjJNFqsF0GtWdxDqTqDVdJYoFPjHcCsj4Tkd6jASrOnoe2qvgViTwtpTgK5ePZNQf4flBx9Gf0VzM4VqVW2oPrYne+2smCNNGmsFIMEvgctNx5gsMxNlZr13VDCGLhSVB2IPNEmsFECv7vwM6M2mc0yVD6ar9gIo3Nq3fN1NMceZFNZdA2g220Z7ph+YbzrL+fDqSCuD5aq/q0Opknz0yv3bh+LOFIV9PUB7ZiNNevABLk7VfDZ0WTHBt+LMMhms6gF09eqZJPQgcJHpLOeLIuwfaj371HB85YlcW+v8rt4/n4k/WXXs6gES+nWa+ODD6B3C2clSrco5Lbnhr8abKBprBNDRLHeazlEPLkpWGw2MofINZZM1+92aIKzpXAvMNR2jHsxIlKN27Lzdy3rXxJcmGnsEED5vOkK9CBiVoBYJ0dvjSxONFQIoCMgNpnPUk/ZEWLNOkRvVkgtwKwSgq2sp8H7TMepJWxBxHQAX7+lYF8s0tomwQwAprzUdod60RfQAAFIOrWizJQIEy0xHqDdp0cidqyJWtNkOAQg/bDpBI0gFtXuBAKxosyUCSNM89ZsKadGadaosjjFKTYwLoJ2drTT53b9aJCMEQJjTv+Tmc6YRxY1xAUgmZ5iO0Cgm2rmD7SdmxhIkAvMCqBrfCY0imGCkny6kjLfdvABBMTHxh5qTie/0lI3PcjYvgKYHTUdoFCER1wBAPhMOxBSlJuYFCMPpK0C1OQEVJEttxucFmBegt3cQyJuO0QjK0QLksn1bR+LKUgvjAggo6JumczSCQrQAr4+23SzGBRglmHZLrwAUNGL3Cla02Q4BhJdNR6g3ZRXKEb9vVemPL01tLBGgvNN0hHozHE4wCAzCHbEEmQA7BKDlOSBnOkU9GS5H3t7IDc4ovRBXliisEEB27MgBz5rOUU+Go87/8Mza0TYbxwoBAFA2m45QT86UavcAAg/HGCUSewRoHdwCTIubQsNhQLH2EHCoJV/8Y5x5orBGANm+fwj0UdM56sHpUtRaFrJ5Sf8Oa0S3RgAAknIPEDmbshk4Ubv7LydCvS/OLBNhlQDyt+ffRNhiOseFMBQG5M5dKQQAgc3LXvzr6zFHisQqAQAQ/R5NPCQ8Wqj5hHckTOimGKNMCusEkB273gB+ZDrH+VAMpXb3r/LDFbt73oo30cRYJwAAQfpe4DXTMabKv4sptPo0kFdmnUlYde4/i5UCjN4Y0s8Cxh+XTpaCCkeLVa/+cxrobYve2GblI28rBQCQnbteQrjbdI7JcjifJqzy6xf0rhV7evYZiDQprBUAQJ55/ucgPzGdYyIGyglOVB376/3ZvT0Pxh5oClgtAAA7e+8GrL1BVFbhYC5zTrnAI9m9Xd80EGlKWC+AQMjx07cDfzCdpRoH8plzZ/4Iv2/Jz/6CsCn6H6IWYL0AANLfX+CSD90CatWCi+8UUpUvjhhD78/2rb5lSf+W6osGWoYVixRMBb161bdR7sXwyuFHiykO5d/zz66CoN/J7u2x/pqlkqYTAP63iPSjwAIT2z9WSnIwl6Zi9x0KNLx1+b6nd5nIcyE0xSlgPLKjdw8tg0uB7wOxdrVHCqnKg19C+WkplbyiGQ8+NGkPUIl2dV1JEN4HXNfI7RTHrvZP/3+q11OIbmy2dwSNp+kFOIuu6VyJyHeBG6lzz3a8lOTtXJoSEiKyNSyXf7Dyxad313Mbppg2ApxFu7vnQvFziGy4kPcJKnCqlOTdQoqRMHhZ4bflUnLzVfu3vV3HuMaZdgJUol1dlxHoWuAToFeMvjha2mt9vqzCcBiUB0rBOydKiV35MHhSk8HfV+7e/q8YY8fKtBagGr9+5IkDLcMj81LFPKl8nvbDR8i1tzE8czYnLvnAwTtu+9R80xnjxNoXMTWKgVkXlQZmVaxIs+g9S/XUWOV5+tKUw0BP/fACOI4XwHG8AI7jBXAc50YBiWQqiY49ppexUbCO/ZFfAuf2h3MNvvjSS6OGen4Y6HELL4DjeAEcxwvgOF4Ax/ECOI4XwHG8AI7jBXAcL4DjeAEcxwvgOF4Ax/ECOI4XwHG8AI7jBXAcL4DjeAEcxwvgOF4Ax/ECOI4XwHG8AI7jBXAcL4DjeAEcxwvgOF4Ax/ECOI4XwHG8AI7jBXAcL4DjeAEcxwvgOF4Ax/ECOI4XwHG8AI7jBXAcL4DjeAEcxwvgOF4Ax/ECOI4XwHG8AI7jBXAcFwUYqFUhcCrOIDbgnAAKx86nbrrinACCvBhRvS+2IJbgnABo+GytKhF5Ls4oNuCcAKGMPAUcHV8uyLHWVLHHQCSjOCfA+iVLCgoPjS9Xwl9cv2hR3kQmkzgnAEA6kb8HOFJR9J+wRe8zlcckTgrw6cWLz4jKbcBB4ICo3Lp+4cLTpnOZ4L/CToUdSeSJdAAAAABJRU5ErkJggg==" }}
                        coordinate={{ latitude: mapLocation.latitude, longitude: mapLocation.longitude }}
                        draggable
                        onDragEnd={(e) => console.log(e.nativeEvent.coordinate, "tet")} />
                </MapView>
            </View> */}
            <View >
                <RBSheet
                    height={210}
                    ref={refRBSheet}
                    closeOnDragDown={true}
                    closeOnPressMask={false}
                    customStyles={{
                        draggableIcon: {
                            backgroundColor: "grey"
                        }
                    }}>
                    <View style={styles.btnContainer}>
                        <TouchableOpacity style={styles.appCurrentButtonContainer}
                            onPress={onCurrentLocation}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <Icon name='my-location' size={24} color='white' style={{ paddingRight: 10 }} />
                                <Text style={styles.btnTextCurrent} >Current Location</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.appNewButtonContainer}
                            onPress={onNewLocation}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <Icon name='location-on' size={24} color='#777' style={{ paddingRight: 10 }} />
                                <Text style={styles.btnTextNew} >New Location</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </RBSheet>
            </View >
        </>
    )
}

export default Location;

