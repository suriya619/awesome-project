import React, { useState, useEffect } from 'react';
import { ScrollView, StyleSheet, Text, useColorScheme, View } from 'react-native';
import { CustomTextField, CustomButton, CustomActivityIndicator } from './CustomComponents/Custom';
import { getProduct } from "../Components/Redux/Action/SigninAction";
import { useDispatch, useSelector } from 'react-redux';
import Location from './Location';

export default function Home(props) {

    const styles = StyleSheet.create({
        container: {
            marginHorizontal: 10,
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center'
        },
        greetings: {
            alignItems: 'flex-end',
            paddingEnd: '10%',
            paddingTop: '10%',
            position: 'absolute',
            direction: 'rtl',
            width: '100%',
        },
        greetingsText: {
            fontSize: 16,
            color: "#009688",
            fontWeight: "normal",
        }
    })
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);
    const { navigation } = props;
    const dispatch = useDispatch();
    const userName = useSelector(state => (state.auth.UsersDataList?.user?.firstName));

    useEffect(() => {
        dispatch(getProduct()).then(() => {
            setLoading(false)
        })
    }, [])


    return (<>
        {userName ? <View style={styles.greetings}><Text style={styles.greetingsText}>Hi, {userName}</Text></View> : null}
        {/* <CustomActivityIndicator animating={isLoading} /> */}
        <View style={styles.container}>
            <CustomButton onPress={() => navigation.navigate('Food')} text={'Food'} />
            <CustomButton onPress={() => navigation.navigate('Liquor')} text={'liquor'} />
            <CustomButton onPress={() => navigation.navigate('About')} text={'About'} />
        </View >
        {isLoading ? null : <Location {...props} />}
    </>
    )
}