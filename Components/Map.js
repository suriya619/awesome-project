import React, { useState, useEffect } from "react";
import { StyleSheet, View, Text, Button, TouchableOpacity } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { useDispatch, useSelector } from 'react-redux';
import { getCountries } from "./Redux/Action/SigninAction";
import { AutocompleteDropdown } from 'react-native-autocomplete-dropdown';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { CustomButton } from './CustomComponents/Custom';

const Map = (props) => {

    const styles = StyleSheet.create({

        map: {
            height: 'auto',
            width: 'auto',
            alignItems: 'stretch',
            alignSelf: 'stretch',
            flex: 1,
        },
        mapContainer: {
            height: 400,
            width: undefined,
            backgroundColor: 'white',
            borderRadius: 5,

        },
        mapTextBox: {
            height: 50,
            width: undefined,
            backgroundColor: 'white',
            alignItems: 'center',
            margin: 20,
            marginBottom: 10,
            borderRadius: 5,
        },
        appButtonContainer: {
            backgroundColor: "#009688",
            justifyContent: "center",
            padding: 16,
            margin: 12,
            borderRadius: 5,
        },
        appButtonText: {
            fontSize: 18,
            color: "#fff",
            fontWeight: "bold",
            alignSelf: "center",
            textTransform: "uppercase",
        },
        helpetText: {
            fontSize: 14,
            color: "grey",
            fontWeight: "normal",
            justifyContent: 'flex-start',
            paddingLeft: 10,
            paddingTop: 10,
        }
    });

    const { navigation } = props;
    console.log(navigation, 'navigation')
    const newLocation = props?.route?.params?.newLocation;
    console.log(newLocation, '1313123')
    const dispatchData = useDispatch();
    useEffect(() => {
        // dispatchData(getCountries())
    }, [])

    const countries = useSelector(state => (state.auth.listCountries))
    const latlng = { latitude: 37.4219983, longitude: -122.084 }
    const [coordinates, setCoordinates] = useState(latlng);
    const [selectedItem, setSelectedItem] = useState('');


    const onSelectItem = (value) => {
        setSelectedItem(value)
        if (value) {
            const tmp = (countries || []).find((item) => {
                if (item.name.common === value.id) {
                    return item
                }
            })
            setCoordinates({ latitude: tmp.latlng[0], longitude: tmp.latlng[1] })
        }
    }

    const newwLocation = () => {
        navigation.navigate('Home')
    }

    return (<>
        {/* <AutocompleteDropdown
            clearOnFocus={false}
            closeOnSubmit={true}
            closeOnBlur={true}
            showChevron={false}
            onSelectItem={item => {
                item && onSelectItem(item)
            }}
            dataSet={(countries || []).map((x) => {
                return {
                    id: x.name.common,
                    title: x.name.common
                }
            })}
            inputContainerStyle={{
                backgroundColor: 'white',
                borderRadius: 5,
                margin: 20,
                marginBottom: 10,
            }}
            textInputProps={{
                placeholder: 'Search a country',
                autoCorrect: false,
                autoCapitalize: 'none',
                style: {
                    borderRadius: 5,
                    backgroundColor: 'white',
                    color: 'grey',
                },
            }}
            suggestionsListContainerStyle={{
                backgroundColor: 'white',
            }}
            inputHeight={50}

        /> */}

        <View>

            {newLocation ?
                <><View style={{
                    flex: 1,
                    padding: 20,
                    justifyContent: 'space-between'
                }}>
                    <View style={styles.mapContainer}>
                        <MapView
                            provider={PROVIDER_GOOGLE}
                            style={styles.map}
                            region={{
                                latitude: latlng.latitude,
                                longitude: latlng.longitude,
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0922,
                            }}>
                            <Marker
                                image={{ uri: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAADsQAAA7EB9YPtSQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAkpSURBVHic7Z17jFxVHcc/vzuvfbRpqQYUU9q0KVYrxXa2DdtuF2qgiTw0JlIIVnwlaERJlDT+YUzqHwrKP0YTxIAmKjZAFdSGVOqipbCUpN2WFDbytq0Uqn1v9zHP+/OP3eqwnbm7287cc2bP+fx5zmTu99z7mXPvuffMucI0Rzs755CkG7gCCRaDXg76PpDZwIwQUqESllRKuTAxkFN562Qx+cxQSXoz5czOpS89edJ0GxqJmA7QCPSazsWEsgH4JPBxIJjaNwiDZeFoIR2eLCf2qeg2leTDK/b85dUGxDXKtBFAs9kUMzIbUL4GrKzX9+Y04HA+zalSAuAFRB6A45s7+vqK9dqGSZpeAM1mU7Sn70BlI8K8Rm3nZCnBoXyGkgrAAUR/DCcfanYRmloA7V7VjXA/ypI4tlcMhTdzGYbCxGiB8JqGeueKfT09cWy/ETSlALpuaTsjM36G8EVibkMI/DPXcvaUAKAIv4KWuzr6tg7HmaUeNJ0A2n3VRyB4DPiYsQzAW7kMp0rJyuJXEF3f0dfzkqFY58UUr47NomtW3QDBHgwefBj91SxoKTAzUa4sXozKrt0d111vKNZ50TQC6JpVGxCeANpMZwEQlAUteTJBWFncLiF/2r1s3VdM5ZoqTSGAdnd+GeE3QMp0lkqSoixoKYw/jyZF9MG+5dd+yUyqqWH9NYBe3XUTGj4OJCf8sCHeLaR4p5AeX1wWdH12b8/jJjJNFqsF0GtWdxDqTqDVdJYoFPjHcCsj4Tkd6jASrOnoe2qvgViTwtpTgK5ePZNQf4flBx9Gf0VzM4VqVW2oPrYne+2smCNNGmsFIMEvgctNx5gsMxNlZr13VDCGLhSVB2IPNEmsFECv7vwM6M2mc0yVD6ar9gIo3Nq3fN1NMceZFNZdA2g220Z7ph+YbzrL+fDqSCuD5aq/q0Opknz0yv3bh+LOFIV9PUB7ZiNNevABLk7VfDZ0WTHBt+LMMhms6gF09eqZJPQgcJHpLOeLIuwfaj371HB85YlcW+v8rt4/n4k/WXXs6gES+nWa+ODD6B3C2clSrco5Lbnhr8abKBprBNDRLHeazlEPLkpWGw2MofINZZM1+92aIKzpXAvMNR2jHsxIlKN27Lzdy3rXxJcmGnsEED5vOkK9CBiVoBYJ0dvjSxONFQIoCMgNpnPUk/ZEWLNOkRvVkgtwKwSgq2sp8H7TMepJWxBxHQAX7+lYF8s0tomwQwAprzUdod60RfQAAFIOrWizJQIEy0xHqDdp0cidqyJWtNkOAQg/bDpBI0gFtXuBAKxosyUCSNM89ZsKadGadaosjjFKTYwLoJ2drTT53b9aJCMEQJjTv+Tmc6YRxY1xAUgmZ5iO0Cgm2rmD7SdmxhIkAvMCqBrfCY0imGCkny6kjLfdvABBMTHxh5qTie/0lI3PcjYvgKYHTUdoFCER1wBAPhMOxBSlJuYFCMPpK0C1OQEVJEttxucFmBegt3cQyJuO0QjK0QLksn1bR+LKUgvjAggo6JumczSCQrQAr4+23SzGBRglmHZLrwAUNGL3Cla02Q4BhJdNR6g3ZRXKEb9vVemPL01tLBGgvNN0hHozHE4wCAzCHbEEmQA7BKDlOSBnOkU9GS5H3t7IDc4ovRBXliisEEB27MgBz5rOUU+Go87/8Mza0TYbxwoBAFA2m45QT86UavcAAg/HGCUSewRoHdwCTIubQsNhQLH2EHCoJV/8Y5x5orBGANm+fwj0UdM56sHpUtRaFrJ5Sf8Oa0S3RgAAknIPEDmbshk4Ubv7LydCvS/OLBNhlQDyt+ffRNhiOseFMBQG5M5dKQQAgc3LXvzr6zFHisQqAQAQ/R5NPCQ8Wqj5hHckTOimGKNMCusEkB273gB+ZDrH+VAMpXb3r/LDFbt73oo30cRYJwAAQfpe4DXTMabKv4sptPo0kFdmnUlYde4/i5UCjN4Y0s8Cxh+XTpaCCkeLVa/+cxrobYve2GblI28rBQCQnbteQrjbdI7JcjifJqzy6xf0rhV7evYZiDQprBUAQJ55/ucgPzGdYyIGyglOVB376/3ZvT0Pxh5oClgtAAA7e+8GrL1BVFbhYC5zTrnAI9m9Xd80EGlKWC+AQMjx07cDfzCdpRoH8plzZ/4Iv2/Jz/6CsCn6H6IWYL0AANLfX+CSD90CatWCi+8UUpUvjhhD78/2rb5lSf+W6osGWoYVixRMBb161bdR7sXwyuFHiykO5d/zz66CoN/J7u2x/pqlkqYTAP63iPSjwAIT2z9WSnIwl6Zi9x0KNLx1+b6nd5nIcyE0xSlgPLKjdw8tg0uB7wOxdrVHCqnKg19C+WkplbyiGQ8+NGkPUIl2dV1JEN4HXNfI7RTHrvZP/3+q11OIbmy2dwSNp+kFOIuu6VyJyHeBG6lzz3a8lOTtXJoSEiKyNSyXf7Dyxad313Mbppg2ApxFu7vnQvFziGy4kPcJKnCqlOTdQoqRMHhZ4bflUnLzVfu3vV3HuMaZdgJUol1dlxHoWuAToFeMvjha2mt9vqzCcBiUB0rBOydKiV35MHhSk8HfV+7e/q8YY8fKtBagGr9+5IkDLcMj81LFPKl8nvbDR8i1tzE8czYnLvnAwTtu+9R80xnjxNoXMTWKgVkXlQZmVaxIs+g9S/XUWOV5+tKUw0BP/fACOI4XwHG8AI7jBXAc50YBiWQqiY49ppexUbCO/ZFfAuf2h3MNvvjSS6OGen4Y6HELL4DjeAEcxwvgOF4Ax/ECOI4XwHG8AI7jBXAcL4DjeAEcxwvgOF4Ax/ECOI4XwHG8AI7jBXAcL4DjeAEcxwvgOF4Ax/ECOI4XwHG8AI7jBXAcL4DjeAEcxwvgOF4Ax/ECOI4XwHG8AI7jBXAcL4DjeAEcxwvgOF4Ax/ECOI4XwHG8AI7jBXAcL4DjeAEcxwvgOF4Ax/ECOI4XwHG8AI7jBXAcFwUYqFUhcCrOIDbgnAAKx86nbrrinACCvBhRvS+2IJbgnABo+GytKhF5Ls4oNuCcAKGMPAUcHV8uyLHWVLHHQCSjOCfA+iVLCgoPjS9Xwl9cv2hR3kQmkzgnAEA6kb8HOFJR9J+wRe8zlcckTgrw6cWLz4jKbcBB4ICo3Lp+4cLTpnOZ4L/CToUdSeSJdAAAAABJRU5ErkJggg==" }}
                                coordinate={{ latitude: latlng.latitude, longitude: latlng.longitude }}
                                draggable
                                onDragEnd={(e) => console.log(e.nativeEvent.coordinate, "tet")} />
                        </MapView>
                    </View>
                    <View style={{ backgroundColor: 'white' }}>
                        <Text style={styles.helpetText} >Marked location will be your delivery location</Text>
                        <TouchableOpacity style={styles.appButtonContainer} onPress={newwLocation} disabled={false} >
                            <Text style={styles.appButtonText} >Confirm</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ height: 50, backgroundColor: 'grey' }}></View>
                    <View style={{ height: 50, backgroundColor: 'grey' }}></View>
                </View>
                </>
                :
                <View style={styles.mapContainer}>
                    <MapView
                        provider={PROVIDER_GOOGLE}
                        style={styles.map}
                        region={{
                            latitude: latlng.latitude,
                            longitude: latlng.longitude,
                            latitudeDelta: 0.05,
                            longitudeDelta: 0.05,
                        }}>
                        <Marker coordinate={{ latitude: latlng.latitude, longitude: latlng.longitude }}></Marker>
                    </MapView>
                </View>
            }

            {/* <MapViewDirections
                origin={origin}
                destination={destination}
                apikey={GOOGLE_MAPS_APIKEY}
                strokeWidth={3}
                strokeColor="hotpink"
            /> */}
        </View>
    </>
    )
}

export default Map