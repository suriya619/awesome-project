import React, { useState, useEffect } from 'react';
import { ScrollView, StyleSheet, Text, View, Button, TextInput, Icon, TouchableOpacity } from 'react-native';
import { CustomTextField, CustomButton, CustomActivityIndicator, CustomCard, CustomProductCard, CustomAddCartProduct } from './CustomComponents/Custom';
import { getFood } from "./Redux/Action/SigninAction";
import { useDispatch, useSelector } from 'react-redux';
import { isEmpty } from 'lodash';


const Menu = (props) => {

    const { navigation } = props;
    const [isLoading, setLoading] = useState(true);
    const dispatchData = useDispatch();
    const food = useSelector(state => (state.auth.FoodDetails))
    console.log(food)
    const styles = StyleSheet.create({
        container: {
            marginHorizontal: 10
        },
    });

    useEffect(() => {
        dispatchData(getFood())
            .then(() => {
                setLoading(false)
            })
    }, [])
    useEffect(() => {
        if (!isEmpty(food)) {
            food?.forEach((element,) => {
                element.price = (Math.floor(100 + Math.random() * 1000));
            })
            // setLoading(false)
            console.log(food, 'food')
        }
    }, [food])

    const result = food.reduce((unique, o) => {
        if (!unique.some(obj => obj.food_pairing[0] === o.food_pairing[0])) {
            unique.push(o);
        }
        return unique;
    }, []);

    const onPressHandler = () => {
        navigation.navigate('Billing Details')
    }


    return (
        <>
            <CustomActivityIndicator animating={isLoading} />
            <ScrollView style={styles.container}>
                {result?.map((x, index) =>
                    <CustomProductCard name={x.food_pairing[0]} price={x.price} key={x.id} product={x} />
                )}
            </ScrollView>
            <CustomAddCartProduct onPress={() => onPressHandler()} />
        </>
    )
}

export default Menu;