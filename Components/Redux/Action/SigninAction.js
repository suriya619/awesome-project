import axios from "axios";
import { AUTH_USER_DETAILS, GET_PRODUCTS, GET_FOOD, GET_INCDEC_PRODUCTS, SET_CART_VIEW, GET_COUNTRIES } from '../index';

const Capture_GetUserDetails = (result) => {
    return {
        type: AUTH_USER_DETAILS,
        payload: result
    };
};
export const SigninUser = (reqObj) => {
    if (reqObj) {
        return async (dispatch) => {
            let apiResponse = axios({
                method: 'POST',
                baseURL: 'http://192.168.0.233:3298/',
                url: `/Account/login`,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify(reqObj)
            }).then((response) => {
                dispatch(Capture_GetUserDetails(response.data));
                return response;
            })
                .catch((err) => {
                    if (err.response) {
                        return err.response?.data;
                    } else {
                        return err
                    }
                });
            return apiResponse;
        };
    }
    return null;
};
export const capture_getProduct = (result) => {
    return {
        type: GET_PRODUCTS,
        payload: result
    }
};
export const getProduct = () => {
    return async (dispatch) => {
        await fetch(`https://api.clubq.app/api/Widget/Wid_GetProductsDetails?VenueId=71`)
            .then(response => response.json())
            .then(response => {
                dispatch(capture_getProduct(response))
            })
    }
};


export const capture_getFood = (result) => {
    return {
        type: GET_FOOD,
        payload: result
    }
};
export const getFood = () => {
    return async (dispatch) => {
        await fetch(`https://api.punkapi.com/v2/beers`)
            .then(response => response.json())
            .then(response => {
                dispatch(capture_getFood(response))
            })
    }
};


export const capture_getProduct_IncDec = (result) => {
    return {
        type: GET_INCDEC_PRODUCTS,
        payload: result
    }
};
export const setCart = (action) => {
    return {
        type: SET_CART_VIEW,
        payload: action
    }
};

export const capture_getCountries = (result) => {
    return {
        type: GET_COUNTRIES,
        payload: result
    }
};
export const getCountries = () => {
    return async (dispatch) => {
        await fetch(`https://restcountries.com/v3.1/all`)
            .then(response => response.json())
            .then(response => {
                dispatch(capture_getCountries(response))
            })
    }
};
