import { AUTH_USER_DETAILS, LOGOUT, GET_PRODUCTS, GET_FOOD, GET_INCDEC_PRODUCTS, SET_CART_VIEW, GET_COUNTRIES } from '../index'

const initialState = {
    UsersDataList: [],
    ProductDetails: [],
    FoodDetails: [],
    ProductDetailsIncDec: [],
    cartView: false,
    listCountries: [],
}


const authReducer = (state = initialState, action) => {
    console.log(state, 'authReducer')
    switch (action.type) {
        case AUTH_USER_DETAILS:
            return {
                ...state,
                UsersDataList: action.payload
            }
        case GET_PRODUCTS:
            return {
                ...state,
                ProductDetails: action.payload
            }
        case GET_FOOD:
            return {
                ...state,
                FoodDetails: action.payload
            }
        case GET_INCDEC_PRODUCTS:
            return {
                ...state,
                ProductDetailsIncDec: action.payload
            }
        case SET_CART_VIEW:
            return {
                ...state,
                cartView: action.payload
            }
        case GET_COUNTRIES:
            return {
                ...state,
                listCountries: action.payload
            }

        case LOGOUT:
            return {
                ...initialState
            }
        default:
            return state;
    }
}
export default authReducer;