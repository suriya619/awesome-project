import React, { useState, useEffect } from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { CustomTextField, CustomButton, CustomActivityIndicator, CustomCard } from './CustomComponents/Custom';
import { getProduct } from "./Redux/Action/SigninAction";
import { useDispatch, useSelector } from 'react-redux';
import { isEmpty } from 'lodash';

export default function Product(props) {

    const { navigation } = props;
    const dispatchData = useDispatch();
    const [isLoading, setLoading] = useState(true);
    const product = useSelector(state => (state.auth.ProductDetails))

    const styles = StyleSheet.create({
        container: {
            marginHorizontal: 10,
            paddingTop: 10,
        },
    });
    useEffect(() => {
        dispatchData(getProduct())
            .then(() => {
                setLoading(false)
            })
    }, [])
    useEffect(() => {
        if (!isEmpty(product)) {
            product?.forEach(element => {
                element.quantity = 0;
                element.total = 0;
            });
        }
    }, [product])


    const result = product.reduce((unique, o) => {
        if (!unique.some(obj => obj.categoryCode === o.categoryCode)) {
            unique.push(o);
        }
        return unique;
    }, []);

    const onPressHandler = (x, index) => {
        const clickProduct = (x.categoryCode)
        const selectArr = product.filter(o => o.categoryCode === clickProduct);
        navigation.navigate('Liquor Details', listArr = { selectArr, clickProduct })
    }
    return (
        <>
            <CustomActivityIndicator animating={isLoading} />  
            <ScrollView style={styles.container}>
                {result.map((x, index) =>
                    <CustomCard text={x.categoryCode} onPress={() => onPressHandler(x, index)} key={x.id} />
                )}
            </ScrollView>
        </>
    )
}