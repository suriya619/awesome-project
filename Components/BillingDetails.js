import React, { useState, useEffect } from "react";
import { StyleSheet, View, Text, ScrollView, Button } from 'react-native';
import { connect, useDispatch } from "react-redux";
import { CustomTextField, CustomButton, CustomActivityIndicator, CustomAddCartProduct, CustomViewCard, CustomPayments, CustomViewCardTotal, CustomCashPayments } from './CustomComponents/Custom';
import { isEmpty } from 'lodash';
import { SigninUser, setComponentType } from './Redux/Action/SigninAction';



const BillingDetails = (props) => {

    const styles = StyleSheet.create({
        container: {
            marginHorizontal: 10,
            flex: 1,
        },
        paymentBillContainer: {
            elevation: 0.5,
            backgroundColor: "white",
            padding: 10,
            borderRadius: 5,
            flexDirection: 'column',
            marginLeft: 10,
            marginRight: 10,
            marginTop: 10,
        },
        onlinePaymentsHeader: {
            padding: 20,
            paddingBottom: 0,
            paddingLeft: 10,
            fontSize: 16,
            color: "#009688",
            fontWeight: "bold",
            textTransform: "uppercase",
            justifyContent: 'flex-start'
        },
    });
    const dispatchData = useDispatch();

    const TotalBill = [
        { id: 1, label: 'item 1', value: '1', price: 240, quantity: 2, palceholder: 'Selected Productes' },
        { id: 2, label: 'item 2', value: '2', price: 40, quantity: 4, palceholder: 'Selected Productes' },
        { id: 3, label: 'item 3', value: '3', price: 520, quantity: 2, palceholder: 'Selected Productes' },
        { id: 4, label: 'item 4', value: '4', price: 240, quantity: 7, palceholder: 'Selected Productes' },
        { id: 5, label: 'item 5', value: '5', price: 80, quantity: 1, palceholder: 'Selected Productes' },
    ];
    const netBanking = [
        { id: 1, label: 'ICICI', value: '1', accNo: 'XXXXXXXXX542', type: 'Net Banking', palceholder: 'Select a Banking' },
        { id: 2, label: 'HDFC', value: '2', accNo: 'XXXXXXXXX731', type: 'Net Banking', palceholder: 'Select a Banking' },
        { id: 3, label: 'SBI', value: '3', accNo: 'XXXXXXXXX851', type: 'Net Banking', palceholder: 'Select a Banking' },
        { id: 4, label: 'AXIS', value: '4', accNo: 'XXXXXXXXX542', type: 'Net Banking', palceholder: 'Select a Banking' },
        { id: 5, label: 'FEDERAL BANK', value: '5', accNo: 'XXXXXXXXX641', type: 'Net Banking', palceholder: 'Select a Banking' },
    ];

    const upiBanking = [
        { id: 1, label: 'PHONE PE', value: '1', accNo: 'XXXXXXXXX542', type: 'UPI', palceholder: 'Select a UPI' },
        { id: 2, label: 'GOOGLE PAY', value: '2', accNo: 'XXXXXXXXX731', type: 'UPI', palceholder: 'Select a UPI' },
        { id: 3, label: 'AMAZON PAY', value: '3', accNo: 'XXXXXXXXX851', type: 'UPI', palceholder: 'Select a UPI' },
        { id: 4, label: 'APPLE PAY', value: '4', accNo: 'XXXXXXXXX542', type: 'UPI', palceholder: 'Select a UPI' },
        { id: 5, label: 'PAYTM', value: '5', accNo: 'XXXXXXXXX641', type: 'UPI', palceholder: 'Select a UPI' },
    ];

    const { navigation } = props;
    const [selectedNetBanking, setSelectedNetBanking] = useState('');
    const [selectedUPI, setSelectedUPI] = useState('');

    const onSelectNetBanking = (item) => {
        setSelectedNetBanking(item)
    }
    const onSelectUPI = (item) => {
        setSelectedUPI(item)
    }
    return (
        <>
            <ScrollView style={styles.container}>
                <Text style={styles.onlinePaymentsHeader}>{TotalBill[0]?.palceholder}</Text>
                <View style={styles.paymentBillContainer}>
                    {TotalBill.map((x, index) =>
                        <CustomViewCard item={x.label} price={x.price} quantity={x.quantity} key={index} />
                    )}
                    <CustomViewCardTotal />
                </View>
                <CustomPayments value={selectedNetBanking} data={netBanking} onSelect={onSelectNetBanking} />
                <CustomPayments value={selectedUPI} data={upiBanking} onSelect={onSelectUPI} />
                <CustomCashPayments />
                <Button title="map" onPress={() => navigation.navigate('Map')}></Button>
            </ScrollView>
        </>
    )
}

export default BillingDetails